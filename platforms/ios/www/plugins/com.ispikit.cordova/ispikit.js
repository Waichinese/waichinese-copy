cordova.define("com.ispikit.cordova.ispikit", function(require, exports, module) { /*global cordova*/
cordova.define("cordova/plugin/ispikit",
    function (require, exports, module) {

        var exec = cordova.require('cordova/exec');

        function init(win, fail) {
            exec(win, fail, "IspikitCordovaPlugin", "init", []);
	}

        function start(arg, win, fail) {
            exec(win, fail, "IspikitCordovaPlugin", "start", [arg]);
	}

        function stop(win, fail) {
            exec(win, fail, "IspikitCordovaPlugin", "stop", []);
	}

        function startPlayback(win, fail) {
            exec(win, fail, "IspikitCordovaPlugin", "startPlayback", []);
	}

        function stopPlayback(win, fail) {
            exec(win, fail, "IspikitCordovaPlugin", "stopPlayback", []);
	}

        module.exports = {
            init: init,
	    start: start,
	    stop: stop,
	    startPlayback: startPlayback,
	    stopPlayback: stopPlayback
        }
    }
);

});
