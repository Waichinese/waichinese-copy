#import <Cordova/CDVPlugin.h>

@interface IspikitCordovaPlugin : CDVPlugin

- (void) init:(CDVInvokedUrlCommand*)command;
- (void) start:(CDVInvokedUrlCommand*)command;
- (void) stop:(CDVInvokedUrlCommand*)command;
- (void) startPlayback:(CDVInvokedUrlCommand*)command;
- (void) stopPlayback:(CDVInvokedUrlCommand*)command;

@end
