#import "IspikitCordovaPlugin.h"
#import "ISTAnalyzer.h"
@implementation IspikitCordovaPlugin

ISTAnalyzer* analyzer;
NSString *initDoneCallbackId;
NSString *newAudioCallbackId;
NSString *playbackDoneCallbackId;
CDVPluginResult* newAudioCallback;
CDVPluginResult* playbackDoneCallback;

- (void) initDoneWithStatus:(NSNumber *)initStatus {
  if ([initStatus shortValue] == 0) {
     CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:result callbackId:initDoneCallbackId];
  } else {
     CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus:CDVCommandStatus_ERROR];
    [self.commandDelegate sendPluginResult:result callbackId:initDoneCallbackId];
  } 

}

- (void) newAudioWithAudio:(NSArray *)audio {
  newAudioCallback = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsMultipart:audio];
  [newAudioCallback setKeepCallback:[NSNumber numberWithBool:YES]];
  [self.commandDelegate sendPluginResult:newAudioCallback callbackId:newAudioCallbackId];
}

- (void) playbackDone:(NSNumber *)x {
  playbackDoneCallback = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
  [newAudioCallback setKeepCallback:[NSNumber numberWithBool:YES]];
  [self.commandDelegate sendPluginResult:playbackDoneCallback callbackId:playbackDoneCallbackId];
}

- (void)init:(CDVInvokedUrlCommand*)command
{
    initDoneCallbackId = [command callbackId];
    analyzer = [ISTAnalyzer new];
    IspikitCordovaPlugin * weakSelf = self;
    [analyzer setInitDoneCallback:^(int initStatus){
        NSNumber * status = [NSNumber numberWithInt:initStatus];
        [weakSelf performSelectorOnMainThread:@selector(initDoneWithStatus:) withObject:status waitUntilDone:NO];
     }];
    [analyzer startInitialization];
}

- (void)start:(CDVInvokedUrlCommand*)command
{
    newAudioCallbackId = [command callbackId];
    NSString* sentence = [[command arguments] objectAtIndex:0];
    IspikitCordovaPlugin * weakSelf = self;
    [analyzer setNewAudioCallback:^(NSArray* audio){
        [weakSelf performSelectorOnMainThread:@selector(newAudioWithAudio:) withObject:audio waitUntilDone:NO];
     }];
     [analyzer setSentence:sentence];
     BOOL output = [analyzer startRecording];
     newAudioCallback = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:output];
     [newAudioCallback setKeepCallback:[NSNumber numberWithBool:YES]];
     [self.commandDelegate sendPluginResult:newAudioCallback callbackId:newAudioCallbackId];
}

- (void)stop:(CDVInvokedUrlCommand*)command
{
  [analyzer stopRecording];
}

- (void)startPlayback:(CDVInvokedUrlCommand*)command
{
    playbackDoneCallbackId = [command callbackId];
    IspikitCordovaPlugin * weakSelf = self;
    [analyzer setPlaybackDoneCallback:^(){
        [weakSelf performSelectorOnMainThread:@selector(playbackDone:) withObject:[NSNumber numberWithInt:0] waitUntilDone:NO];
     }];
     playbackDoneCallback = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
     [playbackDoneCallback setKeepCallback:[NSNumber numberWithBool:YES]];
  [analyzer startPlayback];
}

- (void)stopPlayback:(CDVInvokedUrlCommand*)command
{
  [analyzer stopPlayback];
}


@end
